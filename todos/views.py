from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import ToDoForm, TodoItemForm


def todo_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_list": lists
    }
    return render(request, "todos/list.html", context)


# Create your views here.
def show_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)

    context = {
        "list_object": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = ToDoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("show_todo_list", id=list.id)

    else:
        form = ToDoForm()
        context = {
            "form": form,
        }

    return render(request, "todos/create.html", context)


def edit_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ToDoForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("show_todo_list", id=todo_list.id)

    else:
        form = ToDoForm(instance=todo_list)

    context = {
        "form": form

    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("show_todo_list", todoitem.list.id)

    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }

    return render(request, "todos/items.html", context)


def edit_todo_item(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
   # todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            item = form.save(False)
            item.save()
            return redirect("show_todo_list", id=item.list.id)

    else:
        form = TodoItemForm(instance=todoitem)

    context = {
        "form": form,
        "todoitem": todoitem,

    }
    return render(request, "todos/updateitem.html", context)
