from django.urls import path
from todos.views import todo_list, show_todo_list, create_list, edit_todo_list
from todos.views import todo_list_delete, todo_item_create, edit_todo_item


urlpatterns = [
   path("", todo_list, name="todo_list"),
   path("<int:id>/", show_todo_list, name="show_todo_list"),
   path("create/", create_list, name="create_list"),
   path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
   path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
   path("items/", todo_item_create, name="todo_item_create"),
   path("<int:id>/updateitem/", edit_todo_item, name="edit_todo_item"),

 ]
